var angular = require('angular');

module.exports = angular
  .module('app.contacts.component', ['auth0'])
  .component('contacts', {
    templateUrl: '/app/js/contacts/components/contacts.template.html',
    controller: ContactsController,
  });

function ContactsController(auth, $scope) {
    var ctrl = this;
    $scope.auth = auth;
    $scope.name = auth ? auth.profile.nickname : '';

}

