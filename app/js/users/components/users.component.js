module.exports = angular.module('app.users.component.user_info', ['auth0', 'ngResource', 'ngRoute', 'ui.bootstrap', 'ngAnimate']).component('users', {
    templateUrl : '/app/js/users/components/templates/users.template.html',
    controller  : UsersController,
    transclude  : true
});

function UsersController($scope, auth, $http, $uibModal, $resource) {
    'use strict';
    var ctrl = this;
    $scope.auth                 = auth;
    $scope.animationsEnabled    = true;
    $scope.roles = [
        { name: "User", id: 0 },
        { name: "Admin", id: 1 }
    ];

    ctrl.functionToBePassed = function () {
        $http.get("https://rest.angular.dev/v1/ss/users").success(function(data) {
            ctrl.users = data;
        });
    };

    $scope.create = function () {
        var modalInstance = $uibModal.open({
            animation       : $scope.animationsEnabled,
            templateUrl     : '/app/js/users/components/templates/usersCreate.template.html',
            controller      : ModalCreateInstanceCtrl,
            resolve         : {
                roles: function () {
                    return $scope.roles ;
                }
            }
        });

        modalInstance.result.then(function () {
            ctrl.functionToBePassed();
        });
    };
}

function ModalCreateInstanceCtrl($scope, $uibModalInstance, roles, $http ) {
    $scope.roles        = roles;
    $scope.create       = [];
    $scope.create.role  = $scope.roles[0];

    $scope.submitted = false;
    $scope.createForm = function() {
        if ($scope.create_form.$valid) {
            $http({
                method  : 'post',
                url     : 'https://rest.angular.dev/v1/ss/users',
                data    : {
                    'fName'     : $scope.create.fname,
                    'lName'     : $scope.create.lname,
                    'email'     : $scope.create.email,
                    'active'    : $scope.create.active,
                    'role'      : $scope.create.role.id
                }
            }).success(function(data, status, headers, cfg) {
                $uibModalInstance.close();
            }).error(function(data, status, headers, cfg) {
                alert('Ну йобана! Знов шось не так =(')
            });
        } else {
            $scope.create_form.submitted = true;
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}