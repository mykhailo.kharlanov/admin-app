'use strict';

module.exports = angular.module('app.users.component.user_list', []).component('userListComponent', {
    templateUrl : '/app/js/users/components/templates/usersList.template.html',
    bindings    : {
        users           : '<',
        functionToPass  : '&'
    },
    controller  : UserListController
}).directive('ensureUnique', ['$http', function($http) {
    return {
        require: 'ngModel',
        link: function(scope, ele, attrs, c) {
            scope.$watch(attrs.ngModel, function(v) {
                $http({
                    method  : 'post',
                    url     : 'https://rest.angular.dev/v1/ss/users/unique',
                    data    : {
                        'email' : v
                    }
                }).success(function(data, status, headers, cfg) {
                    if ( data == 1 ) {
                        c.$setValidity('unique', true);
                    } else {
                        c.$setValidity('unique', false);
                    }
                }).error(function(data, status, headers, cfg) {
                    c.$setValidity('unique', false);
                });
            });
        }
    };
}]).directive('ngFocus', [function() {
    var FOCUS_CLASS = "ng-focused";
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function(scope, element, attrs, ctrl) {
            ctrl.$focused = false;
            element.bind('focus', function(evt) {
                element.addClass(FOCUS_CLASS);
                scope.$apply(function() {ctrl.$focused = true;});
            }).bind('blur', function(evt) {
                element.removeClass(FOCUS_CLASS);
                scope.$apply(function() {ctrl.$focused = false;});
            });
        }
    }
}]);

function UserListController( $scope, $http, $uibModal ) {
    var ctrl = this;
    ctrl.functionToPass();

    ctrl.animationsEnabled    = true;
    ctrl.roles = [
        { name: "User", id: 0 },
        { name: "Admin", id: 1 }
    ];

    ctrl.update = function ( id ) {
        $http.get("https://rest.angular.dev/v1/ss/users/" + id).success(function(data) {
            var modalInstance = $uibModal.open({
                animation       : ctrl.animationsEnabled,
                templateUrl     : '/app/js/users/components/templates/usersUpdate.template.html',
                controller      : ModalUpdateInstanceCtrl,
                resolve         : {
                    roles: function () {
                        return ctrl.roles ;
                    },
                    data: function () {
                        return data ;
                    }
                }
            });

            modalInstance.result.then(function () {
                ctrl.functionToPass();
            });
        });
    };

    ctrl.delete = function (id) {
        $http.get("https://rest.angular.dev/v1/ss/users/" + id).success(function(data) {
            var modalInstance = $uibModal.open({
                animation       : ctrl.animationsEnabled,
                templateUrl     : '/app/js/users/components/templates/usersDelete.template.html',
                controller      : ModalDeleteInstanceCtrl,
                resolve         : {
                    roles: function () {
                        return ctrl.roles;
                    },
                    data: function () {
                        return data ;
                    }
                },
            });

            modalInstance.result.then(function () {
                ctrl.functionToPass();
            });
        });
    };
}

function ModalUpdateInstanceCtrl($scope, $http, $uibModalInstance, roles, data) {
    var ctrl = this;

    $scope.roles          = roles;
    $scope.update         = data;
    $scope.update.role    = $scope.roles[data.role];
    $scope.submitted      = false;

    $scope.updateForm = function() {
        if ($scope.update_form.$valid) {
            $http({
                method  : 'put',
                url     : 'https://rest.angular.dev/v1/ss/users/' + data.id,
                data    : {
                    'fName'     : $scope.update.fName,
                    'lName'     : $scope.update.lName,
                    'email'     : $scope.update.email,
                    'active'    : $scope.update.active,
                    'role'      : $scope.update.role.id
                }
            }).success(function(data, status, headers, cfg) {
                $uibModalInstance.close();
            }).error(function(data, status, headers, cfg) {
                alert('Ну йобана! Знов шось не так =(')
            });
        } else {
            $scope.update_form.submitted = true;
        }
    }

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}

function ModalDeleteInstanceCtrl($scope, $uibModalInstance, roles, data, $http) {
    $scope.roles = roles;

    $scope.ok = function () {
        $http({
            method  : 'delete',
            url     : 'https://rest.angular.dev/v1/ss/users/' + data.id,
        }).success(function(data, status, headers, cfg) {
            $uibModalInstance.close();
        }).error(function(data, status, headers, cfg) {
            alert('Ну йобана! Знов шось не так =(')
        });
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
}