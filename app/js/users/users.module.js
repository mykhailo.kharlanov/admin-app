var angular     = require('angular'),
    userInfo    = require('./components/users.component.js'),
    userList    = require('./components/userList.component.js');

module.exports = angular.module("app.users", [
    userInfo.name,
    userList.name
]);