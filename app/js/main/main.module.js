var
    angular           = require('angular'),
    contactsComponent = require('../contacts/contacts.module.js'),
    loginComponent    = require('../login/login.module.js'),
    usersComponent    = require('../users/users.module.js'),
    mainComponent     = require('./components/main.component.js');

module.exports = angular.module("app.main", [
    mainComponent.name,
    loginComponent.name,
    usersComponent.name,
    contactsComponent.name
]);